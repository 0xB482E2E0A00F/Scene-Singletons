﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class IsMovableSubscriber : MonoBehaviour
{
    public /*[HideInInspector]*/ Movables movables;
    public Rigidbody rb = null;

    private void OnValidate()
    {
#if UNITY_EDITOR
        if (Application.isEditor)
        {
            if (rb == null)
            {
                rb = GetComponent<Rigidbody>();
                UnityEditor.EditorUtility.SetDirty(this);
            }

            if (movables == null)
            {
                SceneSingletonInitializationHelper sceneSingletonInitializationHelper = default;

                sceneSingletonInitializationHelper.SetupCardinalSubSystem(this);
                sceneSingletonInitializationHelper.SetupSceneSingleton(ref movables);
                UnityEditor.EditorUtility.SetDirty(this);
            }

            if(movables!=null)
            {
                if(rb!=null)
                {
                    if (movables.rigidbodies.Contains(rb) != true)
                        movables.rigidbodies.Add(rb);
                }
            }
        }
#endif
    }

    private void Reset()
    {
        OnValidate();
    }

    private void OnEnable()
    {
        if (movables != null && movables.rigidbodies.Contains(rb) != true)
            movables.rigidbodies.Add(rb);
    }
    private void OnDisable()
    {
        if(movables!=null && movables.rigidbodies.Contains(rb))
            movables.rigidbodies.Remove(rb);
    }

}
