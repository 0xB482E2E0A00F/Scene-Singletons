﻿using UnityEngine;

public class SceneSingleton : MonoBehaviour
{
    public virtual void OnValidate()
    {
        if (Application.isEditor)
        {
            SceneSingletonReferenceCollector ssRefs = GetComponent<SceneSingletonReferenceCollector>();
            if (ssRefs != null && ssRefs.SceneSingletons.Contains(this) != true)
            {
                Debug.LogWarning($"Warning: {this} has not been added to {ssRefs}, adding now...");
                ssRefs.SceneSingletons.Add(this);
#if UNITY_EDITOR
                UnityEditor.EditorUtility.SetDirty(this);
                UnityEditor.EditorUtility.SetDirty(ssRefs);
#endif
            }
        }
    }
    public virtual void Reset()
    {
        if (Application.isEditor)
        {
            SceneSingletonReferenceCollector ssRefs = GetComponent<SceneSingletonReferenceCollector>();
            if (ssRefs != null && ssRefs.SceneSingletons.Contains(this) != true)
            {
                Debug.LogWarning($"Warning: {this} has not been added to {ssRefs}, adding now...");
                ssRefs.SceneSingletons.Add(this);
#if UNITY_EDITOR
                UnityEditor.EditorUtility.SetDirty(this);
                UnityEditor.EditorUtility.SetDirty(ssRefs);
#endif
            }
        }
    }
}