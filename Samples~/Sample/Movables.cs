﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Movables : SceneSingleton
{
    public List<Rigidbody> rigidbodies = new List<Rigidbody>();

    override public void OnValidate()
    {
        base.OnValidate();
#if UNITY_EDITOR
        if (Application.isEditor)
        {
            List<int> indices = new List<int>();

            for(int i =0;i<rigidbodies.Count;i++)
            {
                if (rigidbodies[i] == null)
                    indices.Add(i);
            }

            foreach(int i in indices)
            {
                rigidbodies.RemoveAt(i);
            }

            if(indices.Count!=0)
            {
                UnityEditor.EditorUtility.SetDirty(this);
            }
        }
#endif
    }
    override public void Reset()
    {
        base.Reset();
        OnValidate();
    }
}
