﻿using System.Collections.Generic;
using UnityEngine;

public class SceneSingletonReferenceCollector : MonoBehaviour
{
    [SerializeField]
    public List<SceneSingleton> SceneSingletons = new List<SceneSingleton>();

    public void TryPopulate<T>(out T sceneSingleton) where T : SceneSingleton
    {
        sceneSingleton = null;
        foreach (SceneSingleton ss in SceneSingletons)
        {
            if (ss is T)
            {
                //Debug.Log("TryPopulate" + " scene: " + gameObject.scene.name);
                sceneSingleton = (T)ss;
            }
        }
    }

    public T GetMBDataObjectOfType<T>() where T : SceneSingleton
    {
        foreach (SceneSingleton ss in SceneSingletons)
        {
            if (ss is T)
            {
                //Debug.Log("GetMBDataObjectOfType" + " scene: " + gameObject.scene.name);
                return (T)ss;
            }
        }
        return null;
    }

    public List<SceneSingleton> GetMBDataObjectsOfType<T>()
    {
        List<SceneSingleton> tempList = new List<SceneSingleton>();
        foreach (SceneSingleton ss in SceneSingletons)
        {
            if (ss is T)
            {
                //Debug.Log("GetMBDataObjectsOfType" + " scene: " + gameObject.scene.name);
                tempList.Add(ss);
            }
        }
        return tempList;
    }
}