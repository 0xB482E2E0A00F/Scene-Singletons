﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
public struct SceneSingletonInitializationHelper
{
    private GameObject CardinalObj;
    private SceneSingletonReferenceCollector sceneSingletonReferenceCollector;
    private MonoBehaviour caller;
    private bool isSetup;
    bool isCardinal;

    public SceneSingletonInitializationHelper(MonoBehaviour callerAkaThis)
    {
        isSetup = default;
        CardinalObj = default;
        sceneSingletonReferenceCollector = default;
        caller = default;
        isCardinal=default;

        SetupCardinalSubSystem(callerAkaThis);
    }

    public void SetupCardinalSubSystem(MonoBehaviour callerAkaThis)
    {
        isSetup = false;
        CardinalObj = null;
        sceneSingletonReferenceCollector = null;
        caller = null;
        if (callerAkaThis.gameObject.scene != new Scene())
        {
            isCardinal = false;
            isSetup = true;
            caller = callerAkaThis;

            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Cardinal Subsystem");

            foreach (GameObject go in gameObjects)
            {
                if (go.scene == caller.gameObject.scene && go.scene != new Scene())
                {
                    CardinalObj = go;
                }
            }
            //cardinalSubsystem = GameObject.Find("Cardinal Subsystem");


            if (CardinalObj != null)
            {
                sceneSingletonReferenceCollector = CardinalObj.GetComponent<SceneSingletonReferenceCollector>();
                if (sceneSingletonReferenceCollector == null)
                {
                    Debug.Log("SceneSingletonReferenceCollector not found in " + CardinalObj);
                }
            }
            else
            {
                Debug.Log("Cardinal Subsystem not found in " + caller + "'s scene: " + caller.gameObject.scene.name);
            }
        }
    }

    public void SetupSceneSingleton<T>(ref T sceneSingleton) where T : SceneSingleton
    {
        if(caller!=null && caller.gameObject.scene != new Scene())
        {
            if (isSetup == false)
            {
                Debug.LogWarning("SceneSingletonInitializationHelper: " + sceneSingleton + "is not set up in::: " + caller);
            }
            else if (CardinalObj != null && sceneSingletonReferenceCollector != null)
            {
                if (sceneSingleton == null && CardinalObj.scene != new Scene())
                {
                    //Debug.Log("SetupMBDO caller: " + caller + " scene: " + caller.gameObject.scene.name);
                    //Debug.Log("SetupMBDO system: " + CardinalObj + " scene: " + CardinalObj.scene.name);
                    if (isCardinal == true)
                        sceneSingletonReferenceCollector.TryPopulate(out sceneSingleton);
                    else if(CardinalObj.scene == caller.gameObject.scene)
                        sceneSingletonReferenceCollector.TryPopulate(out sceneSingleton);

#if UNITY_EDITOR
                    UnityEditor.EditorUtility.SetDirty(caller);
#endif
                }
            }
        }
    }
}