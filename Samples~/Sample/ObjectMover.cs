﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMover : MonoBehaviour
{
    [SerializeField] /*[HideInInspector]*/ Movables isMovableSS;

    private void OnValidate()
    {
#if UNITY_EDITOR
        if (Application.isEditor)
        {
            if (isMovableSS == null)
            {
                SceneSingletonInitializationHelper sceneSingletonInitializationHelper = default;

                sceneSingletonInitializationHelper.SetupCardinalSubSystem(this);
                sceneSingletonInitializationHelper.SetupSceneSingleton(ref isMovableSS);
                UnityEditor.EditorUtility.SetDirty(this);
            }
        }
#endif
    }

    public float speed = 1;
    Vector3 velocity = Vector3.zero;
    // Update is called once per frame
    void Update()
    {
        velocity.z = Time.deltaTime * speed;
        foreach (Rigidbody rb in isMovableSS.rigidbodies)
        {
            rb.MovePosition(rb.position + velocity);
        }
    }
}
